package cat.itb.testingitb;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Button button;
    TextView text;
    EditText editTextUsername;
    Intent intent;
    Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = findViewById(R.id.button);
        text = findViewById(R.id.textview);
        editTextUsername = findViewById(R.id.username);

        bundle = new Bundle();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button.setText("Logged");

                intent = new Intent(MainActivity.this, SecondActivity.class);
                intent.putExtra("username", editTextUsername.getText().toString());
                startActivity(intent);
            }
        });
    }
}
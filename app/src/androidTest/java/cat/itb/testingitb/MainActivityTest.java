package cat.itb.testingitb;

import androidx.test.espresso.ViewAction;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isChecked;
import static androidx.test.espresso.matcher.ViewMatchers.isClickable;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {
    String USER_TO_BE_TYPED = "user";
    String PASS_TO_BE_TYPED = "pass";

    @Rule
    public ActivityScenarioRule<MainActivity> activityScenarioRule
            = new ActivityScenarioRule<>(MainActivity.class);

    @Test
    public void elements_on_MainActivity_are_displayed_correctly(){
        onView(withId(R.id.textview)).check(matches(isDisplayed()));

        onView(withId(R.id.button)).check(matches(isDisplayed()));

    }

    @Test
    public void texts_on_MainActivity_are_correct(){
        onView(withId(R.id.textview)).check(matches(withText("ESTE ES EL TITULO")));
    }

    @Test
    public void on_change_MainActivity(){
        onView(withId(R.id.button)).check(matches(isClickable()));
        onView(withId(R.id.button))
                .perform(click())
                .check(matches(withText("BACK")));
    }

    @Test
    public void login_form_behaviour(){
        onView(withId(R.id.username)).perform(typeText(USER_TO_BE_TYPED), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(typeText(PASS_TO_BE_TYPED), closeSoftKeyboard());
        onView(withId(R.id.button))
                .perform(click())
                .check(matches(withText("Logged")));
    }

    @Test
    public void change_activity(){
        onView(withId(R.id.button))
                .perform(click());

        onView(withId(R.id.secondActivity))
                .check(matches(isDisplayed()));
    }

    @Test
    public void change_and_return_activity(){
        onView(withId(R.id.button))
                .perform(click());

        onView(withId(R.id.secondActivity))
                .check(matches(isDisplayed()));

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.button2))
                .perform(click());

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.mainActivity))
                .check(matches(isDisplayed()));
    }

    @Test
    public void large_test_function(){
        onView(withId(R.id.username))
                .perform(typeText(USER_TO_BE_TYPED), closeSoftKeyboard())
                .check(matches(withText(USER_TO_BE_TYPED)));

        onView(withId(R.id.password))
                .perform(typeText(PASS_TO_BE_TYPED), closeSoftKeyboard())
                .check(matches(withText(PASS_TO_BE_TYPED)));

        onView(withId(R.id.button))
                .check(matches(isClickable()));
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.button))
                .perform(click());

        onView(withId(R.id.secondActivity))
                .check(matches(isDisplayed()));

        onView(withId(R.id.welcome))
                .check(matches(withText("Welcome back "+USER_TO_BE_TYPED)));

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.button2))
                .check(matches(isClickable()));

        onView(withId(R.id.button2))
                .perform(click());

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.mainActivity))
                .check(matches(isDisplayed()));

        onView(withId(R.id.username))
                .check(matches(withText("")));

        onView(withId(R.id.password))
                .check(matches(withText("")));

    }





}
